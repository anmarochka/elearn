--1  
DELETE FROM rental
WHERE inventory_id IN (SELECT inventory_id FROM inventory WHERE film_id = (SELECT film_id FROM film WHERE title = 'Black Panther' LIMIT 1));

DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'Black Panther' LIMIT 1);


--2 

DELETE FROM payment
WHERE customer_id = (SELECT customer_id
    FROM customer
    WHERE first_name = 'Hanna'  AND last_name = 'Marachkina'
    );
	
DELETE FROM rental
WHERE customer_id = (SELECT customer_id
    FROM customer
    WHERE first_name = 'Hanna'  AND  last_name = 'Marachkina'
    );

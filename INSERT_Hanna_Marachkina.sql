--1
INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features, fulltext)
VALUES ('The Grinch', 'A 2018 animated Christmas comedy film adaptation of Dr. Seuss\'s story', 2018, 1, NULL, 14, 4.99, 180, 19.99, 'R', CURRENT_TIMESTAMP, '{"Special Features Here"}', 'Full Text Here');

--2
    ('Benedict', 'Cumberbatch', CURRENT_TIMESTAMP),
    ('Rashida', 'Jones', CURRENT_TIMESTAMP),
    ('Kenan', 'Thompson', CURRENT_TIMESTAMP);

INSERT INTO film_actor (actor_id, film_id, last_update)
VALUES
    ((SELECT actor_id FROM actor WHERE first_name = 'Benedict' ), (SELECT film_id FROM film WHERE title = 'The Grinch'), CURRENT_TIMESTAMP),
    ((SELECT actor_id FROM actor WHERE first_name = 'Rashida' ), (SELECT film_id FROM film WHERE title = 'The Grinch'), CURRENT_TIMESTAMP),
    ((SELECT actor_id FROM actor WHERE first_name = 'Kenan' ), (SELECT film_id FROM film WHERE title = 'The Grinch'), CURRENT_TIMESTAMP);

--3

INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features, fulltext)
VALUES
    ('Spider-Man: Into the Spider-Verse', 'A 2018 animated superhero film', 2018, 1, NULL, 14, 4.99, 120, 19.99, 'R', CURRENT_TIMESTAMP, '{"Special Features Here"}', 'Full Text Here'),
    ('Black Panther', 'A 2018 superhero film', 2018, 2, NULL, 7, 3.99, 105, 14.99, 'R', CURRENT_TIMESTAMP, '{"Special Features Here"}', 'Full Text Here'),
    ('Avengers: Infinity War', 'A 2018 superhero film', 2018, 1, NULL, 10, 2.99, 150, 24.99, 'R', CURRENT_TIMESTAMP, '{"Special Features Here"}', 'Full Text Here');

INSERT INTO inventory (film_id, store_id , last_update)
VALUES
     ((SELECT film_id FROM film WHERE title = 'The Grinch' LIMIT 1 ),  (SELECT store_id FROM store WHERE store_id IN (1, 2) LIMIT 1 ), CURRENT_TIMESTAMP),
     ((SELECT film_id FROM film WHERE title = 'Spider-Man: Into the Spider-Verse' LIMIT 1 ),  (SELECT store_id FROM store WHERE store_id IN (1, 2) LIMIT 1 ), CURRENT_TIMESTAMP),
	 ((SELECT film_id FROM film WHERE title = 'Black Panther' LIMIT 1),   (SELECT store_id FROM store WHERE store_id IN (1, 2) LIMIT 1), CURRENT_TIMESTAMP),
	 ((SELECT film_id FROM film WHERE title = 'Avengers: Infinity War' LIMIT 1),   (SELECT store_id FROM store WHERE store_id IN (1, 2) LIMIT 1), CURRENT_TIMESTAMP);

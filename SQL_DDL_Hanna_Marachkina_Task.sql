DROP TABLE IF EXISTS Area CASCADE;
CREATE TABLE Area (
    area_id SERIAL PRIMARY KEY,
    AreaName VARCHAR(64) NOT NULL
);

DROP TABLE IF EXISTS Countries CASCADE;
CREATE TABLE Countries (
    country_id SERIAL PRIMARY KEY,
    CountryName VARCHAR(64) NOT NULL
);

DROP TABLE IF EXISTS Countries_Location CASCADE;
CREATE TABLE Countries_Location (
    area_id SERIAL,
    country_id SERIAL,
    FOREIGN KEY (area_id) REFERENCES LocationAreas(area_id),
    FOREIGN KEY (country_id) REFERENCES Countries(country_id)
);

DROP TABLE IF EXISTS Mountains CASCADE;
CREATE TABLE Mountains (
    mountain_id SERIAL PRIMARY KEY,
    area_id SERIAL,
    Name VARCHAR(64) NOT NULL,
    Height INT NOT NULL CHECK (Height >= 0) DEFAULT 0,
    FOREIGN KEY (area_id) REFERENCES LocationAreas(area_id)
);

DROP TABLE IF EXISTS Routes CASCADE;
CREATE TABLE Routes (
    route_id SERIAL PRIMARY KEY,
    RouteName VARCHAR(64) NOT NULL,
    DifficultyLevel VARCHAR(64) NOT NULL,
    mountain_id SERIAL,
    FOREIGN KEY (mountain_id) REFERENCES Mountains(mountain_id)
);


DROP TABLE IF EXISTS Routes_Countries CASCADE;
CREATE TABLE Routes_Countries (
    route_id SERIAL,
    country_id SERIAL,
    FOREIGN KEY (route_id) REFERENCES Routes(route_id),
    FOREIGN KEY (country_id) REFERENCES Countries(country_id)
);


DROP TABLE IF EXISTS Climbers CASCADE;
CREATE TABLE Climbers (
    climbers_id SERIAL PRIMARY KEY,
    route_id SERIAL,
    StartDate DATE NOT NULL CHECK (StartDate > '2000-01-01') DEFAULT '2000-01-02',
    EndDate DATE NOT NULL CHECK (EndDate> '2000-01-01') DEFAULT '2000-01-02',
    FOREIGN KEY (route_id) REFERENCES Routes(route_id)
);


DROP TABLE IF EXISTS Addresses CASCADE;
CREATE TABLE Addresses (
    address_id SERIAL PRIMARY KEY,
    country_id SERIAL,
	FOREIGN KEY (country_id) REFERENCES Countries(country_id),
    State VARCHAR(64) NOT NULL,
    City VARCHAR(64) NOT NULL,
    ZipCode INT NOT NULL,
    Number VARCHAR(64) NOT NULL,
    
);


DROP TABLE IF EXISTS Climbers CASCADE;
CREATE TABLE Climbers (
    climber_id SERIAL PRIMARY KEY,
    FirstName VARCHAR(64) NOT NULL,
    LastName VARCHAR(64) NOT NULL,
    Birthday DATE NOT NULL,
    address_id SERIAL,
    FOREIGN KEY (address_id) REFERENCES Addresses(address_id) 
);



DROP TABLE IF EXISTS Equipments CASCADE;
CREATE TABLE Equipments (
    equipment_id SERIAL PRIMARY KEY,
    EquipmentName VARCHAR(64) NOT NULL,
    Description VARCHAR(64) NOT NULL
);



DROP TABLE IF EXISTS ClimberEquipment CASCADE;
CREATE TABLE ClimberEquipment (
    climber_id SERIAL,
    equipment_id SERIAL,
    Quantity INT NOT NULL CHECK (Quantity >= 0) DEFAULT 0,
    FOREIGN KEY (climber_id) REFERENCES Climbers(climber_id),
    FOREIGN KEY (equipment_id) REFERENCES Equipments(equipment_id) 
);

DROP TABLE IF EXISTS ClimbParticipants CASCADE;
CREATE TABLE ClimbParticipants (
    participant_id SERIAL PRIMARY KEY,
    event_id SERIAL,
    climber_id SERIAL,
    FOREIGN KEY (event_id) REFERENCES ClimbingEvents(event_id),
    FOREIGN KEY (climber_id) REFERENCES Climbers(climber_id)
);


-- Insert sample data into LocationAreas table
INSERT INTO LocationAreas (AreaName) VALUES
    ('Everest'),
    ('Sugan'),
    ('Himalayas');

-- Insert sample data into Countries table
INSERT INTO Countries (CountryName) VALUES
    ('Norway'),
    ('Greater Caucasus'),
    ('Turkey');

-- Insert sample data into Countries_LocationAreas table
INSERT INTO Countries_LocationAreas (area_id, country_id) VALUES
    (1, 1),
    (2, 2),
    (3, 3);
	
    -- Insert sample data into Mountains table
INSERT INTO Mountains (area_id, Name, Height) VALUES
    (1, 'Mount Everest', 8848),
    (2, 'Elbrus', 5642),
    (3, 'Makalu', 8485);


-- Insert sample data into Routes table
INSERT INTO Routes (RouteName, DifficultyLevel, mountain_id) VALUES
    ('East', 'Very Extreme', 1),
    ('West Face', 'Difficult', 2),
    ('South Face', 'Extreme', 3);

-- Insert sample data into Routes_Countries table
INSERT INTO Routes_Countries (route_id, country_id) VALUES
    (1, 1),
    (2, 2),
    (3, 3);

-- Insert sample data into ClimbingEvents table
INSERT INTO ClimbingEvents (route_id, StartDate, EndDate) VALUES
    (1, '2023-01-01', '2023-01-03'),
    (2, '2023-02-01', '2023-02-03'),
    (3, '2023-03-01', '2023-03-03');

-- Insert sample data into Addresses table
INSERT INTO Addresses (country_id, State, CityVillage, ZipPostalCode, Street, HouseNumber, FlatNumber) VALUES
    (1, 'Colorado', 'Leadville', 80461, 'Mountain Avenue', 1, 101),
    (2, 'Valais', 'Zermatt', 3920, 'Gornergrat', 2, 202),
    (3, 'Eastern Development Region', 'Namche Bazaar', 56002, 'Sagarmatha Street', 3, 303);

-- Insert sample data into Climbers table
INSERT INTO Climbers (FirstName, LastName, Birthday, address_id) VALUES
    ('Mark', 'Johnson', '1987-10-5', 1),
    ('Anna', 'Muller', '1995-09-2', 2),
    ('Jan', 'Sherpa', '1990-12-21', 3);

-- Insert sample data into Equipment table
INSERT INTO Equipments (EquipmentName, Description) VALUES
    ('Climbing Harness', 'Safety harness for climbing'),
    ('Climbing Helmet', 'Protective helmet for climbing'),
    ('Climbing Rope', 'Dynamic climbing rope for safety');

-- Insert sample data into ClimberEquipment table
INSERT INTO ClimberEquipment (climber_id, equipment_id, Quantity) VALUES
    (1, 1, 1),
    (2, 2, 1),
    (3, 3, 2);

-- Insert sample data into ClimbParticipants table
INSERT INTO ClimbParticipants (event_id, climber_id) VALUES
    (1, 1),
    (2, 2),
    (3, 3);






-- Add 'record_ts' field to LocationAreas table
ALTER TABLE LocationAreas
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in LocationAreas table to set 'record_ts'
UPDATE LocationAreas
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Countries table
ALTER TABLE Countries
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Countries table to set 'record_ts'
UPDATE Countries
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Countries_LocationAreas table
ALTER TABLE Countries_LocationAreas
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Countries_LocationAreas table to set 'record_ts'
UPDATE Countries_LocationAreas
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Mountains table
ALTER TABLE Mountains
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Mountains table to set 'record_ts'
UPDATE Mountains
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Routes table
ALTER TABLE Routes
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Routes table to set 'record_ts'
UPDATE Routes
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Routes_Countries table
ALTER TABLE Routes_Countries
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Routes_Countries table to set 'record_ts'
UPDATE Routes_Countries
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to ClimbingEvents table
ALTER TABLE ClimbingEvents
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in ClimbingEvents table to set 'record_ts'
UPDATE ClimbingEvents
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Addresses table
ALTER TABLE Addresses
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Addresses table to set 'record_ts'
UPDATE Addresses
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Climbers table
ALTER TABLE Climbers
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Climbers table to set 'record_ts'
UPDATE Climbers
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to Equipment table
ALTER TABLE Equipments
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in Equipment table to set 'record_ts'
UPDATE Equipments
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to ClimberEquipment table
ALTER TABLE ClimberEquipment
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in ClimberEquipment table to set 'record_ts'
UPDATE ClimberEquipment
SET record_ts = current_date
WHERE record_ts IS NULL;

-- Add 'record_ts' field to ClimbParticipants table
ALTER TABLE ClimbParticipants
ADD COLUMN record_ts DATE DEFAULT current_date;

-- Update existing rows in ClimbParticipants table to set 'record_ts'
UPDATE ClimbParticipants
SET record_ts = current_date
WHERE record_ts IS NULL;



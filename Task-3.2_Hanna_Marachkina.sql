CREATE TABLE remote_table (
    id serial PRIMARY KEY,
    name VARCHAR(255),
    age INTEGER
);

INSERT INTO remote_table (name, age) VALUES
    ('Hanna', 20),
    ('Dzeya', 15),
    ('Volha', 50);